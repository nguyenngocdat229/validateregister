<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $error = []; // mảng lưu trữ lỗi

        // lấy dữ liệu từ input
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_SPECIAL_CHARS);

        //Validate Email
        if (empty(trim($email))) {
            $error['email']['required'] = 'Email bắt buộc phải nhập';
        } else {
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $error['email']['is_email'] = 'Email không hợp lệ';
            }
        }

        //Validate password
        if (empty(trim($password))) {
            $error['password']['required'] = 'Password bắt buộc phải nhập';
        } else {
            if (!preg_match("^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$^", $password)) {
                $error['password']['is_password'] = 'Password không hợp lệ';
            }
        }
        if (empty($error)) {
            echo 'Validate thành công';
        } else {
            echo 'Validate thất bại';
        }
    }
    ?>

    <h2>Đăng ký tài khoản</h2>
    <form action="" method="post">
        <div>
            <label for="">Email</label>
            <input style="margin-left: 20px;" type="text" name="email" placeholder="Email" value="<?php echo (!empty($_POST['email'])) ? $_POST['email'] : false ?>">
            <?php
            if (!empty($error['email']['required'])) {
                echo '<span style="color: red;">' . $error['email']['required'] . '</span>';
            } else if (!empty($error['email']['is_email'])) {
                echo '<span style="color: red;">' . $error['email']['is_email'] . '</span>';
            }
            ?>
        </div>
        </br>
        <div>
            <label for="">Mật khẩu</label>
            <input type="password" name="password" placeholder="Password" value="<?php echo (!empty($_POST['
            password'])) ? $_POST['password'] : false ?>">
            <?php
            if (!empty($error['password']['required'])) {
                echo '<span style="color: red;">' . $error['password']['required'] . '</span>';
            } else if (!empty($error['password']['is_password'])) {
                echo '<span style="color: red;">' . $error['password']['is_password'] . '</span>';
            }
            ?>
        </div>
        </br>
        <button type="submit">Đăng ký tài khoản</button>
    </form>
</body>

</html>